import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { render } from '@testing-library/react';
import axios from "axios";

const API_PATH = 'http://react-test:8888/index.php';


class App extends React.Component {
	
	//Fields
	constructor(props) {
    	super(props);                     
       	this.state = {                      
        	fname: "",                  
			lname: "",                     
			email: "",  
			number: "",
			dob: "",  
			gender: "",                 
			message: "",
			mailSent: false,
			error: null,     
			errors: {}                
        }                  
	} 

	//Validate and submit form
	handleFormSubmit = e => {
		e.preventDefault();

		let formIsValid = true;
		let errors = {};

		//First Name
        if(!this.state.fname){
			errors["fname"] = "Error name";
			formIsValid = false;
		}

		//Last name
		if(!this.state.lname){
			errors["lname"] = "Error last name";
			formIsValid = false;
		}

		//Email
		if(!this.state.email){
			errors["email"] = "Email is missing.";
			formIsValid = false;
		}

		//Number
		if(!this.state.number){
			errors["number"] = "Number is missing.";
			formIsValid = false;
		}
		else {
			let num = this.state.number;
			if (!this.mobilevalidate(num)){
				formIsValid = false;
				errors["number"] = "Invalid phone number";
			}
		}

		//Date of birth
		if(!this.state.dob){
			errors["dob"] = "Date of birth is missing.";
			formIsValid = false;
		}

		//All errors
		this.setState({ errors: errors });
		
		if (formIsValid) {

			//Submit form
			axios({
				method: 'post',
				url: `${API_PATH}`,
				headers: { 'content-type': 'application/json' },
				data: this.state
			})
			.then(result => {
				this.setState({
				mailSent: result.data.sent
				})
			})
			.catch(error => this.setState({ error: error.message }));
		}

	} 

	//Function to validate number
	mobilevalidate = (text) => {
		const reg = /^[0]?[789]\d{9}$/;
		if (reg.test(text) === false) {
			this.setState({
				mobilevalidate: false,
				telephone: text,
		 	 });
		  	return false;
		} else {
			this.setState({
				mobilevalidate: true,
				telephone: text,
				message: '',
		  });
		  return true;
		}
	}

	render(){
    	return (

      	<div className="App">

			<form noValidate onSubmit={e => this.handleFormSubmit(e)}>

				<div id="accordion">

					{ /* 1. Your details */ }
					<div className="card">
						<div className="card-header" id="headingOne">
							<h5 className="mb-0">
								<a className="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Step 1: Your details
								</a>
							</h5>
						</div>

						<div id="collapseOne" className="collapse show form-section" aria-labelledby="headingOne" data-parent="#accordion">
							<div className="card-body">
								<div className="row">
									<div className="col-4">
										<label>First Name</label>                    
										<input type="text" id="fname" name="firstname" required value={this.state.fname} onChange={e => this.setState({ fname: e.target.value })} />
									</div>
									<div className="col-4">
										<label>Surname</label>
										<input type="text" id="lname" name="lastname" required value={this.state.lname} onChange={e => this.setState({ lname: e.target.value })} />
									</div>
								</div>
								<div className="row mt-3">
									<div className="col-4">
										<label>Email address</label>
										<input type="email" id="email" name="email" required value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
									</div>
								</div>
								<a className="float-right" data-toggle="collapse" data-target="#collapseTwo">Next ></a>
							</div>
						</div>
					</div>

					{ /* 2. More comments */ }
					<div className="card">
						<div className="card-header" id="headingTwo">
							<h5 className="mb-0">
								<a className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								Step 2: More comments
								</a>
							</h5>
						</div>

						<div id="collapseTwo" className="collapse form-section" aria-labelledby="headingTwo" data-parent="#accordion">
							<div className="card-body">
								<div className="row">
									<div className="col-4">
										<label>Telephone number</label>                    
										<input type="tel" id="number" name="number" required value={this.state.number} onChange={e => this.setState({ number: e.target.value })} />
									</div>
									<div className="col-4">
										<label>Gender</label>	
										<select name="gender" id="gender" value={this.state.gender} onChange={e => this.setState({ gender: e.target.value })}>
											<option value="male">Male</option>
											<option value="female">Female</option>
										</select>
									</div>
								</div>
								<div className="row mt-3">
									<div className="col-4">
										<label>Date of birth</label>
										<input type="date" id="dob" name="dob" required value={this.state.dob} onChange={e => this.setState({ dob: e.target.value })} />
									</div>
								</div>
								<a className="float-right" data-toggle="collapse" data-target="#collapseThree">Next ></a>
							</div>
						</div>
					</div>

					{ /* 3. Final comments */ }
					<div className="card">
						<div className="card-header" id="headingThree">
							<h5 className="mb-0">
								<a className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								Step 3: Final comments
								</a>
							</h5>
						</div>
						<div id="collapseThree" className="collapse form-section" aria-labelledby="headingThree" data-parent="#accordion">
							<div className="card-body">
								<div className="row">
									<div className="col-7">
										<label>Comments</label>
										<textarea id="message" name="message" onChange={e => this.setState({ message: e.target.value })} value={this.state.message}></textarea>
									</div>
									<div className="col-3 offset-2 align-self-end">
										<input type="submit" value="Submit" />
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="mt-4">
					{this.state.mailSent && <div>Thank you for your comment.</div>}
					
					{ /* Erros */ }
					<div>
						<p class="error">{this.state.error}</p>
						<p class="error">{this.state.errors['fname']}</p>
						<p class="error">{this.state.errors['lname']}</p>
						<p class="error">{this.state.errors['email']}</p>
						<p class="error">{this.state.errors['number']}</p>
						<p class="error">{this.state.errors['dob']}</p>
						<p class="error">{this.state.errors['message']}</p>
					</div>
				</div> 

		</form>
  
      </div>
    )
  }
}

export default App;
