
<?php
$host = "localhost:8889"; 
$user = "root"; 
$password = "root"; 
$dbname = "reactdb"; 
$id = '';

//Connection
$con = mysqli_connect($host, $user, $password,$dbname);

if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}

// Get data 
$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

if (empty($_POST['fname']) && empty($_POST['email'])) die();

if ($_POST){
    $first_name = $_POST["fname"];
    $last_name = $_POST["lname"];
    $email = $_POST["email"];
    $number = $_POST["number"];
    $message = $_POST["message"];
    $dob = $_POST["dob"];
    $gender = $_POST["gender"];

    //Run SQL
    $sql = "insert into contacts (first_name, last_name, email, number, gender, dob, comments) values ('$first_name', '$last_name', '$email', '$number', '$gender', '$dob', '$message')"; 
    $result = mysqli_query($con,$sql);

    // die if SQL statement failed
    if (!$result) {
        http_response_code(404);
        die(mysqli_error($con));
    }
    else {
        echo json_encode(array(
            "sent" => true
        ));
    }
}
else {
    echo json_encode(["sent" => false, "message" => "Something went wrong"]);
}

$con->close();